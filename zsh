apt update
apt-get install -y software-properties-common python-software-properties
apt install -y emacs
apt install -y zsh
apt install -y wget
apt install -y net-tools
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
apt upgrade
add-apt-repository ppa:deadsnakes/ppa
apt-get update
apt-get install -y python3.6 python3-pip
zsh